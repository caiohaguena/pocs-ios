//
//  ViewController.swift
//  POC
//
//  Created by Caio Hikaru Aguena on 10/02/20.
//  Copyright © 2020 pocman. All rights reserved.
//

import UIKit

class ViewController: MasterViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func shareAction(_ sender: Any) {
        DownloadShareFromUrl()
    }
    
    // download and save
    func DownloadShareFromUrl(){
        var filesToShare = [Any]()
        var components = URLComponents()
        components.scheme = "https"
        components.host = "picsum.photos"
        components.path = "/200/300"
        
        // Create destination URL, hte last component is the file name
        let documentsUrl: URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("imagemR7")
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:components.url!)
        
        // Tenta baixar a parada da URL
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    NSLog("Download feito com sucesso \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                } catch (let writeError) {
                    NSLog("Arquivo ja existe: \(destinationFileUrl) : \(writeError)")
                }
                
            } else {
                NSLog("Erro ao salvar o arquivo")
            }
        }
        
        task.resume()
        
        filesToShare.append(destinationFileUrl)
        let ac = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
        
        self.present(ac, animated: true)
        
        ac.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed:
        Bool, arrayReturnedItems: [Any]?, error: Error?) in
            if completed {
                print("share completed")
                return
            } else {
                print("cancel")
            }
            if let shareError = error {
                print("error while sharing: \(shareError.localizedDescription)")
            }
        }

        
    }
}

