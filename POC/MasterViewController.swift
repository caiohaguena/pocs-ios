//
//  MasterViewController.swift
//  POC
//
//  Created by Caio Hikaru Aguena on 19/02/20.
//  Copyright © 2020 pocman. All rights reserved.
//

import UIKit

open class MasterViewController: UIViewController, UITextFieldDelegate {

    override open func viewDidLoad() {
        super.viewDidLoad()

        // Remove keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        // Add observers to view
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let userInfo = notification.userInfo else { return }
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardFrame = keyboardSize.cgRectValue.height
        
        let selectedTextView = view.getSelectedTextField()
        
        guard let bottonPosition = selectedTextView?.superview?.convert((selectedTextView?.frame.origin)!, to: nil).y else { return }
        
          let screenHeight = UIScreen.main.bounds.height - bottonPosition
        
        if bottonPosition > keyboardSize.cgRectValue.minY {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= (keyboardFrame - screenHeight + (selectedTextView?.frame.height)!)
            }
        }
       
    }
    
    // Make the view return to the center point of the screen, after the keyboard hides
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            view.center = CGPoint(x:
                UIScreen.main.bounds.midX, y:
                UIScreen.main.bounds.midY)
        }
    }

}
